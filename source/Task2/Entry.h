#pragma once
#include "Data.h"
#include "sha256.h"
#include <fstream>
#include <string>
#include <iostream>

using namespace std;

const uint32_t _FOLDER_OFFSET = 0x464f4c44;
const uint32_t _PARENT_OFFSET = 0x524f4f54;
const uint8_t _NAME_LENGTH_MAX = UINT8_MAX;
const uint32_t _ENTRY_SIGNATURE = 0x30454e54;
const uint32_t _DEL_ENTRY_SIGNATURE = 0x21454e54;

class Entry
{

private:
    uint32_t offsetEntry;

    uint32_t signature;
    uint32_t CRC32;
    uint32_t FileSize;
    uint32_t CreatedTime;
    uint32_t ModifiedTime;

    // offset where data start
    // offset = 0 if this entry is folder
    uint32_t offsetData;

    // offset of parent folder in entry
    // offset =0 if parent is root folder
    uint32_t offsetParent;
    string Password;
    string FileName;

    void updateModifiedTime();

public:
    Entry();
    Entry(uint32_t offsetEntry);
    Entry(uint32_t offsetEntry,
          uint32_t signature,
          uint32_t CRC32,
          uint32_t FileSize,
          uint32_t CreatedTime,
          uint32_t ModifiedTime,
          uint32_t offsetData,
          uint32_t offsetParent,
          string Password,
          string FileName);

    static Entry getEntry(fstream &in, streampos startPoint)
    {

        in.seekg(startPoint);
        Entry entry;
        entry.updateSignature(0);

        // uint32_t sig;
        // in.read(reinterpret_cast<char *>(&sig),_SIZE_OF_UINT32_T);
        in.read(reinterpret_cast<char *>(&entry.signature), _SIZE_OF_UINT32_T);
        // cout<<sig;
        // uint32_t crc32;
        // in.read(reinterpret_cast<char *>(&entry.CRC32),_SIZE_OF_UINT32_T);
        // cout<<entry.CRC32<<endl;
        in.read(reinterpret_cast<char *>(&entry.CRC32), _SIZE_OF_UINT32_T);
        in.read(reinterpret_cast<char *>(&entry.FileSize), _SIZE_OF_UINT32_T);
        in.read(reinterpret_cast<char *>(&entry.CreatedTime), _SIZE_OF_UINT32_T);
        in.read(reinterpret_cast<char *>(&entry.ModifiedTime), _SIZE_OF_UINT32_T);
        in.read(reinterpret_cast<char *>(&entry.offsetData), _SIZE_OF_UINT32_T);
        in.read(reinterpret_cast<char *>(&entry.offsetParent), _SIZE_OF_UINT32_T);
        char Pass[65];
        in.read(Pass, 64);
        Pass[64] = '\0';
        // copy to string
        entry.Password = "";
        for (int i = 0; i < 64; i++)
        {
            entry.Password += Pass[i];
        }
        uint8_t nameLength = 0;
        in.read(reinterpret_cast<char *>(&nameLength), sizeof(nameLength));
        char Name[256];
        in.read(Name, nameLength);
        entry.FileName = "";
        for (int i = 0; i < nameLength; i++)
        {
            entry.FileName += Name[i];
        }

        return entry;
    }

    bool compare(const Entry &e);
    bool isUsable();

    // getter
    uint32_t getSignature();
    uint32_t getCRC32();
    uint32_t getFileSize();
    uint32_t getCreatedTime();
    uint32_t getModifiedTime();
    uint32_t getOffsetData();
    uint32_t getOffsetParent();
    uint32_t getOffsetEntry();
    string getPassword();
    string getFileName();

    // update
    void updateOffsetData(uint32_t newOffsetData);
    void updateOffsetParent(uint32_t newOffsetParent);
    void updateFileName(string fileName);
    void updateFileSize(uint32_t fileSize);
    void updateSignature(uint32_t newSignature);
    void updateCRC32(uint32_t crc32);

    // other
    int writeData(fstream &out, streampos pos);
    bool createPassword(fstream &out, string password);
    bool changePassword(fstream &out, string newPassword);
};