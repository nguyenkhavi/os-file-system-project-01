#include "Volume.h"

using namespace std;

bool isFileExist(string fileName)
{
    ifstream in(fileName);
    return in.good();
}

Volume::Volume()
{
    offsetEntry = 0;
    FileSize = 0;
    CreatedTime = ModifiedTime = 0;
    VolumeName = "";
    Password = "";
}

uint32_t Volume::getOffsetStartEntry()
{
    return offsetEntry;
}

uint32_t Volume::getCreatedTime()
{
    return CreatedTime;
}

uint32_t Volume::getModifiedTime()
{
    return ModifiedTime;
}

string Volume::getPassword()
{
    return Password;
}

uint32_t Volume::getCurrentOffsetEntry()
{
    int index = 0;
    uint32_t current_offset = offsetEntry;
    while (index < _NUMBER_SECTOR_ENTRY)
    {
        char *buffer = ReadSector(stream, current_offset);
        if (checkFirstLineEmpty(buffer))
        {
            delete[] buffer;
            return current_offset;
        }
        delete[] buffer;
        current_offset += _SECTOR_SIZE;
        index++;
    }

    return current_offset;
}

int Volume::writeVolumeInfo()
{

    if (stream.is_open())
    {
        stream.seekg(0);
        stream.write((char *)&_SIGNATURE, _SIZE_OF_UINT32_T);
        stream.write((char *)&CreatedTime, _SIZE_OF_UINT32_T);
        stream.write((char *)&ModifiedTime, _SIZE_OF_UINT32_T);
        stream.write((char *)&offsetEntry, _SIZE_OF_UINT32_T);
        stream.write((char *)&FileSize, _SIZE_OF_UINT32_T);

        stream.write(Password.c_str(), 64);

        // write enough sector
        streamsize emptySize = _SECTOR_SIZE - (5 * _SIZE_OF_UINT32_T + 64) % _SECTOR_SIZE;
        writeFullSectorBy0(stream, emptySize, stream.tellp());
        return _SECTOR_SIZE;
    }
    return 0;
}

int Volume::createVolume(string path, int size)
{
    if (!isFileExist(path))
    {
        ofstream out(path, ios::out | ios::binary);
        out.close();
    }
    else
    {
        openVolume(path);
        return SUCCESS;
    }
    stream.open(path, ios::in | ios::out | ios::binary);

    CreatedTime = ModifiedTime = convertCurrentTimeToSecond();
    offsetEntry = _ENTRY_START_POINT;
    offsetDataSection = _DATA_SECTION_START_POINT;
    FileSize = size * 1024 * 1024;
    createPassword("");
    // write volume info
    uint32_t writeByte = writeVolumeInfo();

    // generate section for entry table
    char empty[_SECTOR_SIZE]{0};
    for (int i = 0; i < _NUMBER_SECTOR_ENTRY; i++)
        writeByte += WriteSector(stream, empty, stream.tellp());

    while (writeByte < size * 1024 * 1024)
    {
        writeByte += WriteSector(stream, empty, stream.tellp());
    }
    return SUCCESS;
}

int Volume::openVolume(string path)
{
    if (isFileExist(path) == false)
    {
        return NOT_FOUND_ERR;
    }
    stream.open(path, ios::in | ios::binary | ios::out);
    // get information
    // skip 4 byte signature
    stream.seekg(_SIZE_OF_UINT32_T, ios::beg);
    stream.read((char *)&CreatedTime, _SIZE_OF_UINT32_T);
    stream.read((char *)&ModifiedTime, _SIZE_OF_UINT32_T);
    stream.read((char *)&offsetEntry, _SIZE_OF_UINT32_T);

    stream.read((char *)&FileSize, _SIZE_OF_UINT32_T);
    char Pass[64];
    stream.read(Pass, 64);
    Password = Pass;
    // Remove \0 character in the end
    Password.pop_back();

    if (Password.compare(sha256("")) != 0)
    {
        string input;
        cout << "Input password: ";
        cin >> input;
        if (Password.compare(sha256(input)) != 0)
            return WRONG_PASS_ERR;
    }

    stream.seekg(_ENTRY_START_POINT);
    uint32_t offsetEntry = _ENTRY_START_POINT;
    for (int i = 0; i < _NUMBER_SECTOR_ENTRY; i++)
    {
        char *testBuffer = ReadSector(stream, offsetEntry);
        if (checkFirstLineEmpty(testBuffer))
        {
            delete[] testBuffer;
            break;
        }
        delete[] testBuffer;
        Entry e = Entry::getEntry(stream, offsetEntry);

        entryTable.push_back(e);
        offsetEntry += _SECTOR_SIZE;
    }

    return SUCCESS;
}

int Volume::createPassword(string password)
{
    // use SHA256 hash
    string hashPassword = sha256(password);

    // update password
    Password = hashPassword;

    // update in volume info
    stream.seekg(_SIZE_OF_UINT32_T * 5, ios::beg);
    stream.write(Password.c_str(), 64);

    return SUCCESS;
}

int Volume::changePassword(string newPassword)
{
    string pass = getPassword();
    if (pass.compare(sha256("")) != 0)
    {
        string input;
        cout << "Input password: ";
        cin >> input;
        if (pass.compare(sha256(input)) != 0)
            return WRONG_PASS_ERR;
    }
    // change password
    return createPassword(newPassword);
}

// return -1 if can not write
// return 0 if open file error
int Volume::importFile(string filePath)
{
    uint32_t currentOffset = getCurrentOffsetEntry();

    // check can writable
    if (currentOffset == offsetDataSection)
    {
        return -1;
    }

    // create Entry
    Entry entry(currentOffset);

    // create read stream
    ifstream Rstream(filePath, ios::in | ios::binary);

    // get file size
    Rstream.seekg(0, ios::end);
    uint32_t fileSize = Rstream.tellg();

    Rstream.seekg(0);

    // update entry
    entry.updateFileSize(fileSize);
    entry.updateOffsetParent(_PARENT_OFFSET);
    entry.updateOffsetData(offsetDataSection);
    entry.updateFileName(filePath);
    uint32_t crc32 = generateCRC32(filePath);
    entry.updateCRC32(crc32);

    // write data to volume
    int writeByte = entry.writeData(stream, offsetEntry);
    entry.createPassword(stream, "");
    offsetEntry += writeByte;

    // add to vector
    entryTable.push_back(entry);
    // write data to volume
    stream.seekp(offsetDataSection);
    // cout << "Data pointer:" << stream.tellp();
    uint32_t readByte = 0;
    while (readByte < fileSize)
    {
        char *buffer = new char[_SECTOR_SIZE]{0};
        Rstream.read(buffer, _SECTOR_SIZE);

        stream.write(buffer, _SECTOR_SIZE);
        delete[] buffer;
        readByte += _SECTOR_SIZE;
    }
    offsetDataSection += readByte;
    return SUCCESS;
}

// return -2 if not file of volume
int Volume::exportFile(string fileName, string desPath)
{
    int check = isFileOfVolume(fileName);
    if (check >= 0)
    {
        Entry entry = entryTable.at(check);
        string pass = entry.getPassword();
        if (pass.compare(sha256("")) != 0)
        {
            string input;
            cout << "Input password: ";
            cin >> input;
            if (pass.compare(sha256(input)) != 0)
                return WRONG_PASS_ERR;
        }
        // get offset of file
        uint32_t offsetData = entry.getOffsetData();
        uint32_t fileSize = entry.getFileSize();
        // extract data
        return extractData(stream, offsetData, fileSize, desPath);
    }
    return NOT_FOUND_ERR;
}

int Volume::deleteFile(string fileName)
{
    int check = isFileOfVolume(fileName);
    if (check >= 0)
    {
        Entry entry = entryTable.at(check);
        string pass = entry.getPassword();
        if (pass.compare(sha256("")) != 0)
        {
            string input;
            cout << "Input password: ";
            cin >> input;
            if (pass.compare(sha256(input)) != 0)
            {

                return WRONG_PASS_ERR;
            }
        }
        // update signature
        entryTable.at(check).updateSignature(_DEL_ENTRY_SIGNATURE);
        // remove in entry table
        entryTable.erase(entryTable.begin() + check);
    }
    return SUCCESS;
}

// return position entry of file in volume
// return -1 if not find
int Volume::isFileOfVolume(string fileName)
{
    for (int i = 0; i < entryTable.size(); i++)
    {
        Entry cur = entryTable.at(i);
        if (cur.getOffsetData() != cur.getOffsetEntry())
        {
            if (cur.getFileName().compare(fileName) == 0)
                return i;
        }
    }
    return NOT_FOUND_ERR;
}

void Volume::showListFiles()
{
    if (entryTable.size() == 0)
    {
        cout << "Volume is empty!!!\n";
    }
    for (int i = 0; i < entryTable.size(); i++)
    {
        Entry e = entryTable.at(i);
        cout << "+ " + e.getFileName() + " - " + to_string(e.getFileSize()) << endl;
    }
}

Volume::~Volume()
{
    if (stream.is_open())
    {
        stream.close();
    }
}

// change password entry
int Volume::changePasswordFile(string fileName, string pass)
{
    int check = isFileOfVolume(fileName);
    if (check < 0)
    {
        return NOT_FOUND_ERR;
    }

    Entry e = entryTable.at(check);

    // get entry is done
    e.changePassword(stream, pass);
    entryTable[check] = e;
    return SUCCESS;
}

string Volume::getVolumeName()
{
    return VolumeName;
}
