#include "Shell.h"

void doOpen(Volume &v, int args, char *arg1, char *arg2)
{
    if (args == 2)
    {
        int isOk = v.openVolume(arg1);
        switch (isOk)
        {
        case WRONG_PASS_ERR:
            cout << "Wrong password!!!\n";
            break;
        case NOT_FOUND_ERR:
            cout << "Not found!!!\n";
            break;
        case SUCCESS:
            cout << "Success!!!\n";
            break;
        default:
            cout << "Something went wrong!!!\n";
            break;
        }
        return;
    }
    else
    {
        cout << "Usage: open <vol-name> <pass>\n";
        cout << "OR: open <vol-name> (if it don't have password)\n";
    }
}

void doCreate(Volume &v, int args, char *arg1, char *arg2)
{

    if (args == 3)
    {
        int isOk = v.createVolume(arg1, stoi(arg2));
        switch (isOk)
        {
        case WRONG_PASS_ERR:
            cout << "Wrong password!!!\n";
            break;
        case NOT_FOUND_ERR:
            cout << "Not found!!!\n";
            break;
        case SUCCESS:
            cout << "Success!!!\n";
            break;
        default:
            cout << "Something went wrong!!!\n";
            break;
        }
        return;
    }
    else
    {
        cout << "Usage: create <vol-name> <size>\n";
    }
}

void doList(Volume &v, int args, char *arg1, char *arg2)
{
    if (args != 1)
    {
        cout << "Usage: ls\n";
        return;
    }
    else
        v.showListFiles();
}

void doSetPassword(Volume &v, int args, char *arg1, char *arg2)
{
    // setpassword <file-name> <pass> - Set password for file
    // setpassword <pass> - Set password for volume
    if (args == 2 || args == 3)
    {
        int isOk = args == 3 ? v.changePasswordFile(arg1, arg2) : v.createPassword(arg1);
        switch (isOk)
        {
        case WRONG_PASS_ERR:
            cout << "Wrong password!!!\n";
            break;
        case NOT_FOUND_ERR:
            cout << "Not found!!!\n";
            break;
        case SUCCESS:
            cout << "Success!!!\n";
            break;
        default:
            cout << "Something went wrong!!!\n";
            break;
        }
    }
    else
    {
        cout << "Usage: setpassword <file-name> <pass>\n";
        cout << "OR: setpassword <pass> (set password for volume)\n";
    }
}

void doChangePassword(Volume &v, int args, char *arg1, char *arg2)
{
    // changepassword <file-name> <pass> - Set password for file
    // changepassword <pass> - Set password for volume
    if (args == 2 || args == 3)
    {
        int isOk = args == 3 ? v.changePasswordFile(arg1, arg2) : v.changePassword(arg1);
        switch (isOk)
        {
        case WRONG_PASS_ERR:
            cout << "Wrong password!!!\n";
            break;
        case NOT_FOUND_ERR:
            cout << "Not found!!!\n";
            break;
        case SUCCESS:
            cout << "Success!!!\n";
            break;
        default:
            cout << "Something went wrong!!!\n";
            break;
        }
    }
    else
    {
        cout << "Usage: changepassword <file-name> <pass>\n";
        cout << "OR: changepassword <pass> (change password for volume)\n";
    }
}

void doImport(Volume &v, int args, char *arg1, char *arg2)
{
    if (args == 2)
    {
        int isOk = v.importFile(arg1);
        switch (isOk)
        {
        case WRONG_PASS_ERR:
            cout << "Wrong password!!!\n";
            break;
        case NOT_FOUND_ERR:
            cout << "Not found!!!\n";
            break;
        case SUCCESS:
            cout << "Success!!!\n";
            break;
        default:
            cout << "Something went wrong!!!\n";
            break;
        }
    }
    else
    {
        cout << "Usage:import <file-name>\n";
        cout << "OR:import <file-path>\n";
    }
}
void doExport(Volume &v, int args, char *arg1, char *arg2)
{
    if (args == 3)
    {
        int isOk = v.exportFile(arg1, arg2);
        switch (isOk)
        {
        case WRONG_PASS_ERR:
            cout << "Wrong password!!!\n";
            break;
        case NOT_FOUND_ERR:
            cout << "Not found!!!\n";
            break;
        case SUCCESS:
            cout << "Success!!!\n";
            break;
        default:
            cout << "Something went wrong!!!\n";
            break;
        }
    }
    else
    {
        cout << "Usage:export <file-name> <des-path>\n";
    }
}

void doRemove(Volume &v, int args, char *arg1, char *arg2)
{
    if (args == 2)
    {
        int isOk = v.deleteFile(arg1);
        switch (isOk)
        {
        case WRONG_PASS_ERR:
            cout << "Wrong password!!!\n";
            break;
        case NOT_FOUND_ERR:
            cout << "Not found!!!\n";
            break;
        case SUCCESS:
            cout << "Success!!!\n";
            break;
        default:
            cout << "Something went wrong!!!\n";
            break;
        }
    }
    else
    {
        cout << "Usage:rm <file-name>\n";
    }
}

void doHelp()
{
    cout << "Commands are:\n";
    cout << "  create <volume-name> <size>\n";
    cout << "    + Purpose: Create a new volume.\n";
    cout << "    + <volume-name>: Volume's name (maximum: 256 ascii character).\n";
    cout << "    + <size>: Size of volume  (MB, maximum: 4096, minimum: 100).\n";
    cout << "    + Ex: create MyFS.dat 512\n";

    cout << "  open <volume-name>\n";
    cout << "    + Purpose: Open a volume already exists.\n";
    cout << "    + <volume-name>: Volume's name (maximum: 256 ascii character).\n";

    cout << "  ls\n";
    cout << "    + Purpose: List files in the volume.\n";

    cout << "  import <src-path>\n";
    cout << "    + Purpose: Import a file outside into volume.\n";
    cout << "    + <src-path>: Source directory.\n";
    cout << "    + Ex: import main.cpp\n";

    cout << "  export <file-name> <des-path>\n";
    cout << "    + Purpose: Export a file inside volume to a directory.\n";
    cout << "    + <file-name>: Name of the file inside the volume.\n";
    cout << "    + <des-path>: Destination directory.\n";
    cout << "    + Ex: import main.cpp\n";

    cout << "  rm <file-name>\n";
    cout << "    + Purpose: Remove a file inside volume to a directory.\n";
    cout << "    + <file-name>: Name of the file inside the volume.\n";
    cout << "    + Ex: rm main.cpp\n";

    cout << "  setpassword <password>\n";
    cout << "    + Purpose: Set password for the volume.\n";
    cout << "    + <password>: Password.\n";
    cout << "    + Ex: setpassword pass@123\n";

    cout << "  setpassword <file-name> <password>\n";
    cout << "    + Purpose: Set password for the file inside the volume.\n";
    cout << "    + <file-name>: Name of the file inside the volume.\n";
    cout << "    + <password>: Password.\n";
    cout << "    + Ex: setpassword main.cpp pass@123\n";

    cout << "  changepassword <password>\n";
    cout << "    + Purpose: Change password for the volume.\n";
    cout << "    + <password>: Password.\n";
    cout << "    + Ex: changepassword pass@123\n";

    cout << "  changepassword <file-name> <password>\n";
    cout << "    + Purpose: Change password for the file inside the volume.\n";
    cout << "    + <file-name>: Name of the file inside the volume.\n";
    cout << "    + <password>: Password.\n";
    cout << "    + Ex: changepassword main.cpp pass@123\n";

    cout << "  help\n";
    cout << "    + Purpose: View all commands\n";

    cout << "  exit\n";
    cout << "    + Purpose: Exit the program\n";
}
