#pragma once
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include "Entry.h"
#include "Data.h"
#include "sha256.h"
#include "crc32.h"

#define WRONG_PASS_ERR -1
#define NOT_FOUND_ERR -2

#define SUCCESS 1

using namespace std;

bool isFileExist(string fileName);

class Volume
{
private:
    uint32_t offsetEntry;
    uint32_t offsetDataSection;

    uint32_t FileSize;
    uint32_t CreatedTime;
    uint32_t ModifiedTime;
    vector<Entry> entryTable;
    string Password;
    string VolumeName;

    fstream stream;

public:
    const uint32_t _SIGNATURE = 0x4d594653;

    const int _NUMBER_SECTOR_ENTRY = 1024;
    const uint32_t _ENTRY_START_POINT = _SECTOR_SIZE;
    const uint32_t _DATA_SECTION_START_POINT = _SECTOR_SIZE * _NUMBER_SECTOR_ENTRY;

    Volume();

    int isFileOfVolume(string fileName);
    // getter
    uint32_t getOffsetStartEntry();
    uint32_t getCreatedTime();
    uint32_t getModifiedTime();
    string getPassword();

    uint32_t getCurrentOffsetEntry();

    // other
    int writeVolumeInfo();
    int createVolume(string path, int size);
    int openVolume(string path);

    // create password for Volume
    int createPassword(string password);
    // change password for Volume
    int changePassword(string newPassword);

    int importFile(string filePath);
    int exportFile(string fileName, string desPath);
    int deleteFile(string fileName);
    string getVolumeName();
    int changePasswordFile(string fileName, string pass);

    void showListFiles();

    ~Volume();
};