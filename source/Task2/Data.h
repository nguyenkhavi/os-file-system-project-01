#pragma once
#include <fstream>
#include <string>
#include <iostream>
#define WRONG_PASS_ERR -1
#define NOT_FOUND_ERR -2

#define SUCCESS 1
using namespace std;

const int _SECTOR_SIZE = 512;
#define _SIZE_OF_UINT32_T sizeof(uint32_t)

uint32_t convertCurrentTimeToSecond();

char *ReadSector(fstream &in, streampos pos);
bool isReadable(const fstream &stream, streampos pos);

uint32_t WriteSector(fstream &out, char buffer[_SECTOR_SIZE], streampos pos);
void writeFullSectorBy0(fstream &out, streamsize size, streampos pos);

int extractData(fstream &in, streampos pos, int size, string desPath);

bool checkFirstLineEmpty(char *sector);