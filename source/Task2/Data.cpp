#include <fstream>
#include "Data.h"

using namespace std;

// check stream can read or not
bool isReadable(const fstream &stream, streampos pos)
{
    if (!stream.is_open())
        return false;

    return true;
}

// convert current time to second
uint32_t convertCurrentTimeToSecond()
{
    time_t t = time(0);
    tm *now = localtime(&t);
    return uint32_t(mktime(now));
}

char *ReadSector(fstream &in, streampos pos)
{
    if (!isReadable(in, pos))
    {
        return NULL;
    }

    in.seekg(pos);

    // generate buffer with byte 0
    char *buffer = new char[_SECTOR_SIZE]{0};
    in.read(buffer, _SECTOR_SIZE);

    return buffer;
}
uint32_t WriteSector(fstream &out, char *buffer, streampos pos)
{
    out.seekp(pos);
    out.write(buffer, _SECTOR_SIZE);
    return _SECTOR_SIZE;
}

// write a byte 0 util full 1 sector
void writeFullSectorBy0(fstream &out, streamsize size, streampos pos)
{
    out.seekp(pos);
    char *empty = new char[size]{0};
    out.write(empty, size);
    delete[] empty;
}

// extract data to file with path is desPath
// return -1 if position is wrong
// return 0 if size (can read) < size (want to read)
// return 1 if success
int extractData(fstream &in, streampos pos, int size, string desPath)
{
    if (!isReadable(in, pos))
    {
        return NOT_FOUND_ERR;
    }

    in.seekg(pos);
    // create output stream
    ofstream out(desPath);
    while (size > 0)
    {
        char *buffer = NULL;
        if (size < _SECTOR_SIZE)
        {
            buffer = new char[size];

            in.read(buffer, size);
            out.write(buffer, size);
            pos += size;
            size -= size;

            delete[] buffer;
        }
        else
        {
            buffer = ReadSector(in, pos);
            out.write(buffer, _SECTOR_SIZE);
            size -= _SECTOR_SIZE;
            pos += _SECTOR_SIZE;
            delete[] buffer;
        }
    }

    return SUCCESS;
}
bool checkFirstLineEmpty(char *sector)
{
    if (sector != NULL)
    {
        // check first line allbyte=0
        for (int i = 0; i < 16; i++)
            if (sector[i] != 0)
                return false;

        return true;
    }
    return false;
}


