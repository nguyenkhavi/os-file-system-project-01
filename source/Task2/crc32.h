#include <iostream>
#include <string>

using namespace std;

unsigned char *memorymap_file_open(const char *_filename, int *sz);
void memorymap_file_close(void *data, unsigned int size);
unsigned long CRC32_BlockChecksum(const void *data, int length);
uint32_t generateCRC32(string fileName);
