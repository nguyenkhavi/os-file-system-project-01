#include "Entry.h"

using namespace std;

Entry::Entry()
{
    offsetEntry = 0;
    signature = _ENTRY_SIGNATURE;
    CRC32 = 0;
    FileSize = 0;
    CreatedTime = 0;
    ModifiedTime = 0;
    offsetData = _FOLDER_OFFSET;
    offsetParent = _PARENT_OFFSET;
    Password = "";
    FileName = "";
}
Entry::Entry(uint32_t offsetEntry)
{

    this->offsetEntry = offsetEntry;
    signature = _ENTRY_SIGNATURE;
    CreatedTime = ModifiedTime = convertCurrentTimeToSecond();
    offsetData = 0;
    offsetParent = 0;
    Password = "";
    FileName = "";
}

bool Entry::compare(const Entry &e)
{
    return (FileName == e.FileName) && (offsetData == e.offsetData) && (offsetEntry == e.offsetEntry);
}

// return if signature of entry not equal deleted flag
bool Entry::isUsable()
{
    return signature == _DEL_ENTRY_SIGNATURE;
}
uint32_t Entry::getSignature()
{
    return signature;
}

uint32_t Entry::getCRC32()
{
    return CRC32;
}

uint32_t Entry::getFileSize()
{
    return FileSize;
}

uint32_t Entry::getCreatedTime()
{
    return CreatedTime;
}

uint32_t Entry::getModifiedTime()
{
    return ModifiedTime;
}

uint32_t Entry::getOffsetData()
{
    return offsetData;
}

uint32_t Entry::getOffsetParent()
{
    return offsetParent;
}

uint32_t Entry::getOffsetEntry()
{
    return offsetEntry;
}

string Entry::getPassword()
{

    return Password;
}

string Entry::getFileName()
{
    return FileName;
}

void Entry::updateModifiedTime()
{
    ModifiedTime = convertCurrentTimeToSecond();
}

void Entry::updateOffsetData(uint32_t newOffsetData)
{
    offsetData = newOffsetData;
    // update modifiedTime after change
    updateModifiedTime();
}

void Entry::updateOffsetParent(uint32_t newOffsetParent)
{
    offsetParent = newOffsetParent;
    // update modifiedTime after change
    updateModifiedTime();
}

void Entry::updateFileName(string fileName)
{
    FileName = fileName;
}

void Entry::updateFileSize(uint32_t fileSize)
{
    FileSize = fileSize;
}

void Entry::updateSignature(uint32_t newSignature)
{
    if (newSignature != _DEL_ENTRY_SIGNATURE && newSignature != _ENTRY_SIGNATURE)
    {
    }
    signature = newSignature;
}

void Entry::updateCRC32(uint32_t crc32)
{
    CRC32 = crc32;
}

// return number byte written
int Entry::writeData(fstream &out, streampos pos)
{
    out.seekp(pos);
    out.write((char *)&signature, _SIZE_OF_UINT32_T);
    out.write((char *)&CRC32, _SIZE_OF_UINT32_T);
    out.write((char *)&FileSize, _SIZE_OF_UINT32_T);
    out.write((char *)&CreatedTime, _SIZE_OF_UINT32_T);
    out.write((char *)&ModifiedTime, _SIZE_OF_UINT32_T);
    out.write((char *)&offsetData, _SIZE_OF_UINT32_T);
    out.write((char *)&offsetParent, _SIZE_OF_UINT32_T);
    out.write(Password.c_str(), 64);
    uint8_t nameLength = FileName.length();
    out.write((char *)&nameLength, sizeof(nameLength));
    out.write(FileName.c_str(), nameLength);
    return _SECTOR_SIZE;
}

bool Entry::createPassword(fstream &out, string password)
{
    // update password
    Password = sha256(password);
    // update in entry in volume
    out.seekp(offsetEntry + 7 * _SIZE_OF_UINT32_T);
    out.write(Password.c_str(), 64);
    writeData(out, offsetEntry);
    return true;
}

bool Entry::changePassword(fstream &out, string newPassword)
{
    // check password outside
    return createPassword(out, newPassword);
}