// g++ *.cpp -o exe && ./exe
#include <stdexcept>
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include "Volume.h"
#include "Shell.h"
using namespace std;
// Macros

#define streq(a, b) (strcmp((a), (b)) == 0)

int main(int argc, char *argv[])
{
    Volume vol;
    // if (argc != 3) {
    // 	fprintf(stderr, "Usage: %s <diskfile> <nblocks>\n", argv[0]);
    // 	return EXIT_FAILURE;
    // }

    // try {
    // 	disk.open(argv[1], atoi(argv[2]));
    // } catch (std::runtime_error &e) {
    // 	fprintf(stderr, "Unable to open disk %s: %s\n", argv[1], e.what());
    // 	return EXIT_FAILURE;
    // }
    doHelp();
    bool isShowAddr = true;
    while (true)
    {
        char line[BUFSIZ], cmd[BUFSIZ], arg1[BUFSIZ], arg2[BUFSIZ];
        if (isShowAddr)
            cout << "MyFS@FS:~$ ";

        fflush(stderr);

        if (fgets(line, BUFSIZ, stdin) == NULL)
        {
            break;
        }

        int args = sscanf(line, "%s %s %s", cmd, arg1, arg2);

        if (args <= 0)
        {
            isShowAddr = false;
            continue;
        }
        else
        {
            isShowAddr = true;
        }

        if (streq(cmd, "create"))
        {
            doCreate(vol, args, arg1, arg2);
        }
        else if (streq(cmd, "open"))
        {
            doOpen(vol, args, arg1, arg2);
        }
        else if (streq(cmd, "ls"))
        {
            doList(vol, args, arg1, arg2);
        }
        else if (streq(cmd, "setpassword"))
        {
            doSetPassword(vol, args, arg1, arg2);
        }
        else if (streq(cmd, "changepassword"))
        {
            doChangePassword(vol, args, arg1, arg2);
        }
        else if (streq(cmd, "import"))
        {
            doImport(vol, args, arg1, arg2);
        }
        else if (streq(cmd, "export"))
        {
            doExport(vol, args, arg1, arg2);
        }
        else if (streq(cmd, "rm"))
        {
            doRemove(vol, args, arg1, arg2);
        }
        else if (streq(cmd, "help"))
        {
            doHelp();
        }
        else if (streq(cmd, "exit") || streq(cmd, "quit"))
        {
            break;
        }
        else
        {
            printf("Unknown command: %s", line);
            printf("Type 'help' for a list of commands.\n");
        }
        // else if (streq(cmd, "format")) {
        //     do_format(disk, fs, args, arg1, arg2);
        // } else if (streq(cmd, "mount")) {
        //     do_mount(disk, fs, args, arg1, arg2);
        // } else if (streq(cmd, "help")) {
        //     do_help(disk, fs, args, arg1, arg2);
        // } else if (streq(cmd, "password")) {
        //     do_password(disk, fs, args, arg1, arg2);
        // } else if (streq(cmd, "mkdir")) {
        //     do_mkdir(disk, fs, args, arg1, arg2);
        // } else if (streq(cmd, "rmdir")) {
        //     do_rmdir(disk, fs, args, arg1, arg2);
        // } else if (streq(cmd, "touch")) {
        //     do_touch(disk, fs, args, arg1, arg2);
        // } else if (streq(cmd, "rm")) {
        //     do_rm(disk, fs, args, arg1, arg2);
        // } else if (streq(cmd, "cd")) {
        //     do_cd(disk, fs, args, arg1, arg2);
        // } else if (streq(cmd, "ls")) {
        //     do_ls(disk, fs, args, arg1, arg2);
        // } else if (streq(cmd, "stat")) {
        //     do_stat(disk, fs, args, arg1, arg2);
        // } else if (streq(cmd, "copyout")) {
        //     do_file_copyout(disk, fs, args, arg1, arg2);
        // } else if (streq(cmd, "copyin")) {
        //     do_file_copyin(disk, fs, args, arg1, arg2);
    }

    return EXIT_SUCCESS;
}
