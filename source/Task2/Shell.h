#include "Volume.h"

void doOpen(Volume &v, int args, char *arg1, char *arg2);
void doCreate(Volume &v, int args, char *arg1, char *arg2);
void doHelp();
void doImport(Volume &v, int args, char *arg1, char *arg2);
void doExport(Volume &v, int args, char *arg1, char *arg2);
void doList(Volume &v, int args, char *arg1, char *arg2);
void doRemove(Volume &v, int args, char *arg1, char *arg2);
void doSetPassword(Volume &v, int args, char *arg1, char *arg2);
void doChangePassword(Volume &v, int args, char *arg1, char *arg2);
// void do_format(Disk &disk, FileSystem &fs, int args, char *arg1, char *arg2);
// void do_mount(Disk &disk, FileSystem &fs, int args, char *arg1, char *arg2);
// void do_help(Disk &disk, FileSystem &fs, int args, char *arg1, char *arg2);
// void do_password(Disk &disk, FileSystem &fs, int args, char *arg1, char *arg2);
// void do_mkdir(Disk &disk, FileSystem &fs, int args, char *arg1, char *arg2);
// void do_rmdir(Disk &disk, FileSystem &fs, int args, char *arg1, char *arg2);
// void do_touch(Disk &disk, FileSystem &fs, int args, char *arg1, char *arg2);
// void do_rm(Disk &disk, FileSystem &fs, int args, char *arg1, char *arg2);
// void do_file_copyout(Disk &disk, FileSystem &fs, int args, char *arg1, char *arg2);
// void do_file_copyin(Disk &disk, FileSystem &fs, int args, char *arg1, char *arg2);
// void do_cd(Disk &disk, FileSystem &fs, int args, char *arg1, char *arg2);
// void do_ls(Disk &disk, FileSystem &fs, int args, char *arg1, char *arg2);
// void do_stat(Disk &disk, FileSystem &fs, int args, char *arg1, char *arg2);