#include <iostream>
#include <fstream>
#include <string>
//  1 sector = 512 byte
// 1 Cluster = 8 sector
const int bytePerSector = 512;
const int sectorPerCluster = 8;

using namespace std;
void writeData(int k)
{
    ofstream fout;
    int numCluster = (2 - k % 2);
    int numByte = numCluster * sectorPerCluster * bytePerSector;
    string fileName = "E:\\F" + to_string(k) + ".dat";
    // disk E save file F<k> (Window FAT32)
    fout.open(fileName, ios::out | ios::binary);
    int count = 0;
    while (numByte > count)
    {
        fout.write(reinterpret_cast<char *>(&k), sizeof(k));
        count += sizeof(k);
    }
    fout.close();
}
int main()
{

    int n = 0;
    cout << "Enter N : ";
    cin >> n;
    for (int i = 0; i <= n; ++i)
    {
        writeData(i);
        cout << "F" + to_string(i) + ".dat was created successfully" << endl;
    }
    return 0;
}