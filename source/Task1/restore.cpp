//IDE Clion JetBrain
//run as admin
//restore file disk E:
#include <windows.h>
#include <iostream>
#include <vector>
using namespace std;

class Helper
{
public:
    static int convertInt(BYTE sector[], int n, int offset, int numbyte)
    {
        if (offset + numbyte >= n)
            return -1;
        int result = 0;

        for (int i = offset + numbyte - 1; i >= offset; i--)
        {
            result = result * 256 + sector[i];
        }
        return result;
    }
    static int getVal(char c)
    {
        if (c >= '0' && c <= '9')
            return (int)c - '0';
        else
            return (int)c - 'A' + 10;
    }

    static string convert10To16(unsigned long long num, int base = 16, int numByte = 4)
    {
        if (base < 2 || base > 16)
            return "Unsupported base!";
        if (num == 0)
            return "0";

        string result;
        while (num)
        {
            int digit = num % base;
            num = num / base;

            switch (digit)
            {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                result = to_string(digit) + result;
                break;
            case 10:
                result = "A" + result;
                break;
            case 11:
                result = "B" + result;
                break;
            case 12:
                result = "C" + result;
                break;
            case 13:
                result = "D" + result;
                break;
            case 14:
                result = "E" + result;
                break;
            case 15:
                result = "F" + result;
                break;
            }
        }
        while (result.length() < numByte * 2)
        {
            result = "0" + result;
        }
        return result;
    }

    static int convert16To10(string str)
    {
        int base = 16;
        int len = str.length();
        int power = 1; // Initialize power of base
        int num = 0;   // Initialize result
        int i;

        // Decimal equivalent is str[len-1]*1 +
        // str[len-2]*base + str[len-3]*(base^2) + ...
        for (i = len - 1; i >= 0; i--)
        {
            // A digit in input number must be
            // less than number's base
            int value = Helper::getVal(str[i]);
            if (value >= base)
            {
                cout << getVal(str[i]) << endl;

                printf("Invalid Number");
                return -1;
            }

            num += value * power;
            power = power * base;
        }

        return num;
    }
};

class Volume
{
private:
    HANDLE hDevice;
    int SB;
    int SC;
    int NF;
    int SF;
    int SV;
    BYTE *bootSector = new BYTE[512];
    BYTE *FAT = nullptr;
    BYTE *entryTable = nullptr;
    bool readFAT(BYTE *FAT)
    {
        DWORD byteread;
        //FAT start at SB*512 byte, and length is SF * 512 byte
        SetFilePointer(hDevice, SB * 512, NULL, FILE_BEGIN);
        return ReadFile(hDevice, FAT, SF * 512, &byteread, NULL);
    }
    bool readEntryTable(BYTE *EntryTable)
    {
        DWORD byteread;
        //Entry table and Data start at SB + NF * SF byte, length is SC * 512(1 cluster)
        SetFilePointer(hDevice, (SB + NF * SF) * 512, NULL, FILE_BEGIN);
        return ReadFile(hDevice, EntryTable, SC * 512, &byteread, NULL);
    }
    bool writeFATs(BYTE *FAT)
    {
        DWORD bytewrite;
        //FAT start at SB*512 byte, and length is SF * 512 byte
        // SetFilePointer(hDevice, SB * 512, NULL, FILE_BEGIN);
        // return WriteFile(hDevice, FAT, SF * 512, &bytewrite, NULL);

        //Ghi đè bản FAT mới và tất cả bảng FAT
        bool isOk = true;
        for (int i = 0; i < NF; i++)
        {
            SetFilePointer(hDevice, (SB + i * SF) * 512, NULL, FILE_BEGIN);
            if (!WriteFile(hDevice, FAT, SF * 512, &bytewrite, NULL))
            {
                isOk = false;
            }
        }
        return isOk;
    }
    bool writeEntryTable(BYTE *entryable)
    {
        DWORD bytewrite;
        //Entry table and Data start at SB + NF * SF byte, length is SC * 512(1 cluster)
        SetFilePointer(hDevice, (SB + NF * SF) * 512, NULL, FILE_BEGIN);
        return WriteFile(hDevice, entryable, SC * 512, &bytewrite, NULL);
    }

public:
    // muon sua o dia khac o day!!
    Volume()
    {
        const CHAR *nameDisk = "\\\\.\\E:";
        hDevice = CreateFile(TEXT(nameDisk), GENERIC_READ | GENERIC_WRITE,
                             FILE_SHARE_READ | FILE_SHARE_WRITE,
                             NULL, OPEN_EXISTING, 0, NULL);
        if (hDevice == INVALID_HANDLE_VALUE)
        {
            cout << "[ERROR] Cant open device!!!";
            exit(1);
        }
        DWORD bytewrite;
        DWORD byteread;

        SetFilePointer(hDevice, 0, NULL, FILE_BEGIN);
        ReadFile(hDevice, bootSector, 512, &byteread, NULL);
        SC = Helper::convertInt(bootSector, 512, 0x0d, 1);
        SB = Helper::convertInt(bootSector, 512, 0x0e, 2);
        NF = Helper::convertInt(bootSector, 512, 0x10, 1);
        SF = Helper::convertInt(bootSector, 515, 0x24, 4);
        SV = Helper::convertInt(bootSector, 515, 0x20, 4);

        FAT = new BYTE[SF * 512];
        if (!readFAT(FAT))
        {
            cout << "[ERROR] Cant read FAT!!!";
            exit(1);
        }

        entryTable = new BYTE[SC * 512];
        if (!readEntryTable(entryTable))
        {
            cout << "[ERROR] Cant read entry table!!!";
            exit(1);
        }
    };

    bool restoreAllDeletedFiles()
    {
        //Duyệt qua mỗi entry
        for (int i = 0; i < 512 * SC; i += 32)
        {
            //Nếu byte đầu tiên của entry = E5h = 229d
            if (entryTable[i] == 229)
            {
                //vị trí cluster bắt đầu
                int startPos = 0;

                //đổi giá trị byte đầu thành giá trị bất kì khác E5h (Ở đây ta gán ký tự F (70))
                entryTable[i] = 70;

                //đọc vị trí cluster bắt đầu tại 2 byte word cao (14h) và 2 byte word thấp (1A) và biến startPos
                for (int j = i + 20 + 2 - 1; j >= 20 + i; j--)
                {
                    startPos = startPos * 256 + entryTable[j];
                }
                for (int j = i + 26 + 2 - 1; j >= 26 + i; j--)
                {
                    startPos = startPos * 256 + entryTable[j];
                }

                //số lượng sector của nội dung file
                int numSector = 0;
                //Đọc số lượng sector chứa nội dung của file  (4 byte tại 1Ch)
                for (int j = i + 28 + 4 - 1; j >= 28 + i; j--)
                {
                    numSector = numSector * 256 + entryTable[j];
                }
                //số lượng cluster của nội dung file
                int numCluster = numSector / (512 * 8);

                //duyệt qua mỗi cluster chứa nội dung file bắt đầu từ startPos đến vị trí kế cuối (kế cluster cuối file)
                for (int j = 0; j < numCluster - 1; j++)
                {
                    //với mỗi vị trí cluster chứa nội dung file khác cluster cuối (vị trí cluster EOF),
                    //ta gán giá trị cho mỗi vị trí là vị trí kế tiếp
                    //Ví dụ: vị trí cluster là 5 thì ta gán giá trị cho nó là 6d = 00000006h
                    int nextOffset = startPos + j + 1;
                    string hexNextOffser = Helper::convert10To16(nextOffset);
                    int byte1 = Helper::convert16To10(hexNextOffser.substr(0, 2));
                    int byte2 = Helper::convert16To10(hexNextOffser.substr(2, 2));
                    int byte3 = Helper::convert16To10(hexNextOffser.substr(4, 2));
                    int byte4 = Helper::convert16To10(hexNextOffser.substr(6, 2));
                    FAT[(startPos + j) * 4] = byte4;
                    FAT[(startPos + j) * 4 + 1] = byte3;
                    FAT[(startPos + j) * 4 + 2] = byte2;
                    FAT[(startPos + j) * 4 + 3] = byte1;
                }
                //gán giá trị cho vị trí cluster chứa nội dung file cuối cùng là EOF (0FFFFFFFd)
                FAT[(startPos + numCluster - 1) * 4] = 255;
                FAT[(startPos + numCluster - 1) * 4 + 1] = 255;
                FAT[(startPos + numCluster - 1) * 4 + 2] = 255;
                FAT[(startPos + numCluster - 1) * 4 + 3] = 15;
            }
        }
        //khai báo byte đã đọc
        DWORD byteread;
        // Khóa đĩa để ghi đè
        DeviceIoControl(hDevice, (DWORD)FSCTL_LOCK_VOLUME, NULL, 0, NULL, 0, &byteread, NULL);
        DeviceIoControl(hDevice, (DWORD)FSCTL_ALLOW_EXTENDED_DASD_IO, NULL, 0, NULL, 0, &byteread, NULL);

        //ghi bảng entry sau quá trình phục hồi
        if (!writeEntryTable(entryTable))
        {
            cout << "[ERROR] Cant write entry table!!!";
            exit(1);
        }

        //ghi bảng FAT sau quá trình phục hồi

        if (!writeFATs(FAT))
        {
            cout << "[ERROR] Cant write FAT!!!";
            exit(1);
        }
        //sau khi ghi xong thì mở khóa volume
        DeviceIoControl(hDevice, (DWORD)FSCTL_UNLOCK_VOLUME, NULL, 0, NULL, 0, &byteread, NULL);
        return 1;
    }

    ~Volume()
    {
        if (bootSector != nullptr)
            delete[] bootSector;
        if (FAT != nullptr)
            delete[] FAT;
        if (entryTable != nullptr)
            delete[] entryTable;
        CloseHandle(hDevice);
    };
};

int main(int argc, char *argv[])
{
    Volume volume = Volume();
    volume.restoreAllDeletedFiles();
    cout << "Restore all files in first cluster of RDET successfully!\n";
    return 0;
}
